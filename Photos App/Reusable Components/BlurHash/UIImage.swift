//
//  UIImage.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/14/21.
//

import Foundation
import UIKit

extension UIImage {
    convenience init?(blurHash: String, size: CGSize, punch: Float = 1) {
        guard blurHash.count >= 6 else { return nil }

        let sizeFlag = String(blurHash[0]).decode83()
        let numY = (sizeFlag / 9) + 1
        let numX = (sizeFlag % 9) + 1

        let quantisedMaximumValue = String(blurHash[1]).decode83()
        let maximumValue = Float(quantisedMaximumValue + 1) / 166

        guard blurHash.count == 4 + 2 * numX * numY else { return nil }

        let colours: [(Float, Float, Float)] = (0 ..< numX * numY).map { identifier in
            if identifier == 0 {
                let value = String(blurHash[2 ..< 6]).decode83()
                return decodeDC(value)
            } else {
                let value = String(blurHash[4 + identifier * 2 ..< 4 + identifier * 2 + 2]).decode83()
                return decodeAC(value, maximumValue: maximumValue * punch)
            }
        }

        let width = Int(size.width)
        let height = Int(size.height)
        let bytesPerRow = width * 3
        guard let data = CFDataCreateMutable(kCFAllocatorDefault, bytesPerRow * height) else { return nil }
        CFDataSetLength(data, bytesPerRow * height)
        guard let pixels = CFDataGetMutableBytePtr(data) else { return nil }

        for element in 0 ..< height {
            for item in 0 ..< width {
                var rParameter: Float = 0
                var gParameter: Float = 0
                var bParameter: Float = 0

                for jParameter in 0 ..< numY {
                    for iParameter in 0 ..< numX {
                        let basis = cos(Float.pi * Float(item) * Float(iParameter) / Float(width)) * cos(Float.pi * Float(element) * Float(jParameter) / Float(height))
                        let colour = colours[iParameter + jParameter * numX]
                        rParameter += colour.0 * basis
                        gParameter += colour.1 * basis
                        bParameter += colour.2 * basis
                    }
                }

                let intR = UInt8(linearTosRGB(rParameter))
                let intG = UInt8(linearTosRGB(gParameter))
                let intB = UInt8(linearTosRGB(bParameter))

                pixels[3 * item + 0 + element * bytesPerRow] = intR
                pixels[3 * item + 1 + element * bytesPerRow] = intG
                pixels[3 * item + 2 + element * bytesPerRow] = intB
            }
        }

        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)

        guard let provider = CGDataProvider(data: data) else { return nil }
        guard let cgImage = CGImage(width: width,
                                    height: height,
                                    bitsPerComponent: 8,
                                    bitsPerPixel: 24,
                                    bytesPerRow: bytesPerRow,
                                    space: CGColorSpaceCreateDeviceRGB(),
                                    bitmapInfo: bitmapInfo,
                                    provider: provider,
                                    decode: nil,
                                    shouldInterpolate: true,
                                    intent: .defaultIntent)
        else {
            return nil
        }
        self.init(cgImage: cgImage)
    }
}

private func decodeDC(_ value: Int) -> (Float, Float, Float) {
    let intR = value >> 16
    let intG = (value >> 8) & 255
    let intB = value & 255
    return (sRGBToLinear(intR), sRGBToLinear(intG), sRGBToLinear(intB))
}

private func decodeAC(_ value: Int, maximumValue: Float) -> (Float, Float, Float) {
    let quantR = value / (19 * 19)
    let quantG = (value / 19) % 19
    let quantB = value % 19

    let rgb = (
        signPow((Float(quantR) - 9) / 9, 2) * maximumValue,
        signPow((Float(quantG) - 9) / 9, 2) * maximumValue,
        signPow((Float(quantB) - 9) / 9, 2) * maximumValue
    )

    return rgb
}

private func signPow(_ value: Float, _ exp: Float) -> Float {
    copysign(pow(abs(value), exp), value)
}

private func linearTosRGB(_ value: Float) -> Int {
    let vParameter = max(0, min(1, value))
    if vParameter <= 0.0031308 {
        return Int(vParameter * 12.92 * 255 + 0.5)
    } else {
        return Int((1.055 * pow(vParameter, 1 / 2.4) - 0.055) * 255 + 0.5)
    }
}

private func sRGBToLinear<Type: BinaryInteger>(_ value: Type) -> Float {
    let vParameter = Float(Int64(value)) / 255
    if vParameter <= 0.04045 {
        return vParameter / 12.92
    } else {
        return pow((vParameter + 0.055) / 1.055, 2.4)
    }
}

private let encodeCharacters: [String] = {
    return "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#$%*+,-.:;=?@[]^_{|}~".map { String($0) }
}()

private let decodeCharacters: [String: Int] = {
    var dict: [String: Int] = [:]
    for (index, character) in encodeCharacters.enumerated() {
        dict[character] = index
    }
    return dict
}()

extension String {
    func decode83() -> Int {
        var value: Int = 0
        for character in self {
            if let digit = decodeCharacters[String(character)] {
                value = value * 83 + digit
            }
        }
        return value
    }
}

extension String {
    subscript (offset: Int) -> Character {
        self[index(startIndex, offsetBy: offset)]
    }

    subscript (bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start...end]
    }

    subscript (bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start..<end]
    }
}
