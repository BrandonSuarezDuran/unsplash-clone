//
//  EndPoint.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/7/21.
//

import Foundation

enum EndPoint {
    case collections
    case listTopics
    case getATopicsPhotos(id: String)
    case photos
    case photoWithPage(page: String)
    case photoID(id: String)
    case topicsSearch((topic: String, page: String))
    case searchPhoto((query: String, page: String))
    case searchCollection((query: String, page: String))
    case searchUser((query: String, page: String))
    case getACollectionsPhotos(id: String)
    case getAUsersPublicProfile(username: String)
    case listAUsersPhotos(username: String)
    case listAUsersLikedPhotos(username: String)
    case listAUsersCollections(username: String)
}

extension EndPoint: EndPointType {
    
    var task: HTTPTask {
            switch self {
            case .collections:
                return .requestParameters((bodyParameters: nil,
                                           urlParameters: ["per_page": "20", "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
            
            case .listTopics:
                return .requestParameters((bodyParameters: nil, urlParameters: ["per_page": "15", "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .photos:
                return .requestParameters((bodyParameters: nil, urlParameters: ["client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .photoWithPage(page: let page):
                return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "per_page": "40", "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .photoID:
                return .requestParameters((bodyParameters: nil, urlParameters: ["client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .topicsSearch(let (_, page)):
                return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "per_page": "40", "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .searchCollection(let (query, page)):
                return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "per_page": "40", "query": query, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .searchUser(let (query, page)):
                return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "per_page": "40", "query": query, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .searchPhoto(let (query, page)):
                return .requestParameters((bodyParameters: nil, urlParameters: ["page": page, "per_page": "40", "query": query, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .getACollectionsPhotos(id: let id):
                return .requestParameters((bodyParameters: nil, urlParameters: ["id": id, "page": "1", "per_page": "40", "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .getAUsersPublicProfile(username: let username):
                return .requestParameters((bodyParameters: nil, urlParameters: ["username": username, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .getATopicsPhotos(id: let id): // slug or topic ID
                return .requestParameters((bodyParameters: nil, urlParameters: ["id_or_slug": id, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .listAUsersPhotos(username: let username):
                return .requestParameters((bodyParameters: nil, urlParameters: ["username": username, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .listAUsersLikedPhotos(username: let username):
                return .requestParameters((bodyParameters: nil, urlParameters: ["username": username, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
                
            case .listAUsersCollections(username: let username):
                return .requestParameters((bodyParameters: nil, urlParameters: ["username": username, "client_id": "HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q"]))
            }
        }
    
    var path: String {
        switch self {
        case .collections:
            return "collections/"
        
        case .listTopics:
            return "topics/"
            
        case .photos:
            return "photos/"
            
        case .topicsSearch(let (topic, _)):
            return "topics/\(topic)/photos/"
            
        case .searchPhoto:
            return "search/photos"
            
        case .searchCollection:
            return "search/collections"
            
        case .searchUser:
            return "search/users"
            
        case .photoWithPage:
            return "photos/"
            
        case .photoID(let id):
            return "photos/\(id)/"
            
        case .getACollectionsPhotos(let id):
            return "collections/\(id)/photos"
            
        case .getAUsersPublicProfile(username: let username):
            return "/users/\(username)"
            
        case .getATopicsPhotos(id: let id):
            return "/topics/\(id)/photos"
            
        case .listAUsersPhotos(username: let username):
            return "/users/\(username)/photos"
            
        case .listAUsersLikedPhotos(username: let username):
            return "/users/\(username)/likes"
            
        case .listAUsersCollections(username: let username):
            return "/users/\(username)/collections"
        }
    }
}

// MARK: - KEY: HUr7CAIeshOOQ6q0r-XURcy18uTnk8xcG9s0CwDVc5Q
