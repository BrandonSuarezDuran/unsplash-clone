//
//  DecorationsUIView.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/5/21.
//

import UIKit

protocol DecorationsUIView {
    func makeRound(with radius: Double)
}

protocol DecorationsUIButton {
    func makeRound(with radius: Double)
}
