//
//  HTTPMethod.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
