//
//  UIView.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/5/21.
//

import UIKit

extension UIView: DecorationsUIView {
    func makeRound(with radius: Double) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.height / CGFloat(radius)
    }
}
