//
//  FirstRowTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/16/21.
//

import UIKit

class FirstRowTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var authorPhoto: UIImageView! {
        didSet {
            self.authorPhoto.layer.masksToBounds = true
            self.authorPhoto.layer.cornerRadius = self.frame.height / 1.0
            self.authorPhoto.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenWidth)
        }
    }
    @IBOutlet private var backGroundImage: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    let screenWidth = Int(UIScreen.main.bounds.width)
    // MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    // MARK: - Configure
    func configure(placeHolder: String, profilePhoto: String, name: String) {
        self.authorPhoto.downloadImage(with: profilePhoto)
        self.backGroundImage.image = UIImage(blurHash: placeHolder, size: CGSize(width: screenWidth, height: screenWidth))
        self.nameLabel.text = name
    }
}
