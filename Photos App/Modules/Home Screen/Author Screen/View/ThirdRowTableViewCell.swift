//
//  ThirdRowTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/16/21.
//

import UIKit

class ThirdRowTableViewCell: UITableViewCell, SecondCellDelegate {

    @IBOutlet private weak var photosTableView: UITableView! {
        didSet {
            self.photosTableView.delegate = self
            self.photosTableView.dataSource = self
            self.photosTableView.register(UINib(nibName: "SearchPhotoTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchPhotoTableViewCell")
            self.photosTableView.register(UINib(nibName: "SearchCollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCollectionTableViewCell")
        }
    }
    var segmentedControllerIndex: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func didPassSegmentedControlIndex(index: Int) {
        segmentedControllerIndex = index
    }
}

extension ThirdRowTableViewCell: UITableViewDelegate {
}

extension ThirdRowTableViewCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        guard let photoCell = photosTableView.dequeueReusableCell(withIdentifier: "SearchPhotoTableViewCell", for: indexPath) as? SearchPhotoTableViewCell else {
            fatalError("Author Screen Photo/Like cell Not dequeued")
        }
        guard let collectionCell = photosTableView.dequeueReusableCell(withIdentifier: "SearchCollectionTableViewCell", for: indexPath) as? SearchCollectionTableViewCell
        else {
            fatalError("Author Screen Collection cell Not dequeued")
        }
        
        switch segmentedControllerIndex {
        case 0:
            photoCell.configure(photoUrl: "https://images.unsplash.com/photo-1544176161-1f4656f1812f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max", name: "Sample")
            cell = photoCell
            
        case 1:
            cell = photoCell
            
        case 2:
            cell = collectionCell
            
        default:
            photoCell.configure(photoUrl: "https://images.unsplash.com/photo-1544176161-1f4656f1812f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max", name: "Sample")
            cell = photoCell
        }
        return cell
    }
}
