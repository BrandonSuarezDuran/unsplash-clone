//
//  SecondRowTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/16/21.
//

import UIKit

protocol SecondCellDelegate: AnyObject {
    func didPassSegmentedControlIndex(index: Int)
}

class SecondRowTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var segmentedControl: UISegmentedControl! {
        didSet {
            self.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
            self.segmentedControl.selectedSegmentTintColor = UIColor(cgColor: CGColor(red: 100 / 255, green: 99 / 255, blue: 104 / 255, alpha: 1))
            self.segmentedControl.backgroundColor = UIColor(cgColor: CGColor(red: 41 / 255, green: 41 / 255, blue: 44 / 255, alpha: 1))
        }
    }
    
    weak var delegate: SecondCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction private func didChangeIndex(_ sender: UISegmentedControl) {
        let index = segmentedControl.selectedSegmentIndex
        self.delegate?.didPassSegmentedControlIndex(index: index)
    }
}
