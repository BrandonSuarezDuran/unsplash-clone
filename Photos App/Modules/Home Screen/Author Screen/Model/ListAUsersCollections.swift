//
//  ListAUsersCollections.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/7/21.
//

import Foundation

struct ListAUsersCollections: Decodable {
    var coverPhoto: CoverPhoto?
    
    enum CodingKeys: String, CodingKey {
        case coverPhoto = "cover_photo"
    }
}
