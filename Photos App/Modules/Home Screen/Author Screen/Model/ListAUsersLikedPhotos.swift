//
//  ListAUsersLikedPhotos.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/7/21.
//

import Foundation

struct ListAUsersLikedPhotos: Decodable {
    var height: Int?
    var blurHash: String?
    var user: ListAUsersLikedPhotosUser?
    var urls: Urls?
    
    enum CodingKeys: String, CodingKey {
        case height, urls, user
        case blurHash = "blur_hash"
    }
}

struct ListAUsersLikedPhotosUser: Decodable {
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case name
    }
}
