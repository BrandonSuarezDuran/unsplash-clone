//
//  ListAUsersPhotos.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/7/21.
//

import Foundation

struct ListAUsersPhotos: Decodable {
    var height: Int?
    var blurHash: String?
    var urls: Urls?
    
    enum CodingKeys: String, CodingKey {
        case height, urls
        case blurHash = "blur_hash"
    }
}
