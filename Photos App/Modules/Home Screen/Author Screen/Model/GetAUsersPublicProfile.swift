//
//  GetAUsersPublicProfile.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/7/21.
//

import Foundation
import UIKit

struct GetAUsersPublicProfile: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var location: String?
    var profileImage: ProfileImage?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, location
        case profileImage = "profile_image"
    }
}
