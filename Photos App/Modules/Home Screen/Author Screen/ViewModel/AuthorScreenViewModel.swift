//
//  AuthorScreenViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/16/21.
//

import Foundation
import UIKit

protocol AuthorScreenDelegate: AnyObject {
    func reload()
}

class AuthorScreenViewModel {
    // MARK: - Constants
    private let routerInstance = Router<EndPoint>()
    // MARK: - Variables
    private var dataSource: GetAUsersPublicProfile
    private var photosDataSource: [ListAUsersPhotos]
    private var likedPhotoDatasource: [ListAUsersLikedPhotos]
    private var collectionDataSource: [ListAUsersCollections]
    
    weak var delegate: AuthorScreenDelegate?
    var userName: String
    // MARK: - Init
    init(userName: String, delegate: AuthorScreenDelegate) {
        self.dataSource = GetAUsersPublicProfile()
        self.userName = userName
        self.photosDataSource = []
        self.likedPhotoDatasource = []
        self.collectionDataSource = []
        self.delegate = delegate
    }
    // MARK: - Functions
    func fetchUserData() {
        routerInstance.request(.collections) { (result: Result< GetAUsersPublicProfile, AppError>) in
            switch result {
            case .success(let data):
                self.dataSource = data
                
            case .failure(let error):
                print(error)
            }
        }
    }
    // MARK: - Private functions
    
    func fetchPhotos() {
        routerInstance.request(.listAUsersPhotos(username: self.userName)) { [weak self] (result: Result<[ListAUsersPhotos], AppError>) in
            switch result {
            case .success(let data):
                self?.photosDataSource = data
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getRowsForPhotos() -> Int {
        photosDataSource.count
    }
    
    func getPhotos(at index: Int) -> String {
        guard let photo = photosDataSource[index].urls?.small else {
            return ""
        }
        return photo
    }
    
    func fetchLikedPhotos() {
        routerInstance.request(.listAUsersPhotos(username: self.userName)) { [weak self] (result: Result<[ListAUsersLikedPhotos], AppError>) in
            switch result {
            case .success(let data):
                self?.likedPhotoDatasource = data
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getRowsForLikes() -> Int {
        likedPhotoDatasource.count
    }
    
    func getLikedPhotos(at index: Int) -> String {
        guard let photo = likedPhotoDatasource[index].urls?.small else {
            return ""
        }
        return photo
    }
    
    func getLikedPhotosName(at index: Int) -> String {
        guard let name = likedPhotoDatasource[index].user?.name else {
            return ""
        }
        return name
    }
    
    func fetchCollections() {
        routerInstance.request(.listAUsersPhotos(username: self.userName)) { [weak self] (result: Result<[ListAUsersCollections], AppError>) in
            switch result {
            case .success(let data):
                self?.collectionDataSource = data
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getRowsForCollections() -> Int {
        collectionDataSource.count
    }
    
    func getCollectionCoverPhoto(at index: Int) -> String {
        guard let photo = collectionDataSource[index].coverPhoto?.urls?.small else {
            return ""
        }
        return photo
    }
    
    func getCollectionName(at index: Int) -> String {
        guard let name = collectionDataSource[index].coverPhoto?.urls?.small else {
            return ""
        }
        return name
    }
}
