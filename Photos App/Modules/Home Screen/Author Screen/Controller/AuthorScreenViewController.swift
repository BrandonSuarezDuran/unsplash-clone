//
//  AuthorScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/16/21.
//

import UIKit

class AuthorScreenViewController: UIViewController, AuthorScreenDelegate {
    // MARK: - Outlets
    @IBOutlet private weak var goBackButton: UIButton!
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private var backGroundImage: UIImageView!
    @IBOutlet private weak var authorPhoto: UIImageView! {
        didSet {
            self.authorPhoto.layer.masksToBounds = true
            self.authorPhoto.layer.cornerRadius = authorPhoto.frame.height / 1.0
        }
    }
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private weak var segmentedControl: UISegmentedControl! {
        didSet {
            self.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
            self.segmentedControl.selectedSegmentTintColor = UIColor(cgColor: CGColor(red: 100 / 255, green: 99 / 255, blue: 104 / 255, alpha: 1))
            self.segmentedControl.backgroundColor = UIColor(cgColor: CGColor(red: 41 / 255, green: 41 / 255, blue: 44 / 255, alpha: 1))
        }
    }
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(UINib(nibName: "SearchCollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCollectionTableViewCell")
            self.tableView.register(UINib(nibName: "SearchPhotoTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchPhotoTableViewCell")
        }
    }
    // MARK: - Constants
    let screenWidth = Int(UIScreen.main.bounds.width)
    // MARK: - Variables
    lazy var viewModel = AuthorScreenViewModel(userName: name, delegate: self)
    var name: String = ""
    var profilePictureUrl: String = ""
    var userName: String = ""
    var blurHash: String = ""
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        placeBlurHash(with: blurHash)
        nameLabel.text = name
        authorPhoto.downloadImage(with: profilePictureUrl)
        viewModel.fetchUserData()
    }
    
    func reload() {
        tableView.reloadData()
    }

    func placeBlurHash(with placeholder: String) {
        self.backGroundImage.image = UIImage(blurHash: placeholder, size: CGSize(width: screenWidth, height: screenWidth))
    }

    @IBAction private func dismissModalView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func didChangeSegment(_ sender: UISegmentedControl) {
        let segmentedControlIndex = segmentedControl.selectedSegmentIndex
        switch segmentedControlIndex {
        case 1:
            viewModel.fetchPhotos()
            reload()
        
        case 2:
            viewModel.fetchLikedPhotos()
            reload()
            
        case 3:
            viewModel.fetchCollections()
            reload()
            
        default:
            viewModel.fetchPhotos()
            reload()
        }
    }
}
    // MARK: - Table View Delegate
extension AuthorScreenViewController: UITableViewDelegate {
}
    // MARK: - Table View Data Source
extension AuthorScreenViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let segmentedControlIndex = segmentedControl.selectedSegmentIndex
        switch segmentedControlIndex {
        case 1:
            return viewModel.getRowsForPhotos()
            
        case 2:
            return viewModel.getRowsForLikes()
            
        case 3:
            return viewModel.getRowsForCollections()
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let segmentedControllerIndex = segmentedControl.selectedSegmentIndex
        var cell = UITableViewCell()
        guard let photoCell = tableView.dequeueReusableCell(withIdentifier: "SearchPhotoTableViewCell") as? SearchPhotoTableViewCell else {
            fatalError("Author's Screen Photo/Like Cell Not Dequeued")
        }
        guard let collectionCell = tableView.dequeueReusableCell(withIdentifier: "SearchCollectionTableViewCell") as? SearchCollectionTableViewCell else {
            fatalError("Author's Screen Collection Cell Not Dequeeud")
        }
        
        switch segmentedControllerIndex {
        
        case 1:
            photoCell.configure(photoUrl: viewModel.getPhotos(at: index), name: "")
            cell = photoCell
            
        case 2:
            photoCell.configure(photoUrl: viewModel.getLikedPhotos(at: index), name: viewModel.getLikedPhotosName(at: index))
            cell = photoCell
            
        case 3:
            collectionCell.configure(photoUrl: viewModel.getCollectionCoverPhoto(at: index), collectionName: viewModel.getCollectionName(at: index))
            cell = collectionCell
            
        default:
            break
        }
        return cell
    }
}
