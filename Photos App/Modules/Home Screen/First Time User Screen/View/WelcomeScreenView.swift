//
//  FirstTimeUserView.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/5/21.
//

import UIKit

class WelcomeScreenView: UIView {
    @IBOutlet private weak var slidingImage: UIImageView!
    @IBOutlet private weak var startBrowsingButton: UIButton! {
        didSet {
            self.makeRound(with: 40.0)
        }
    }
    
    func configure(with image: String) {
        self.slidingImage.image = UIImage(named: image)
    }
}
