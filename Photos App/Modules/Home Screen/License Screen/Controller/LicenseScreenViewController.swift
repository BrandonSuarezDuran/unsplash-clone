//
//  LicenseScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/13/21.
//

import UIKit

class LicenseScreenViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func openUrl(url: String) {
        if let urlLink = URL(string: url) {
            UIApplication.shared.open(urlLink)
        }
    }
    
    @IBAction private func openActivityActionBar() {
        let shareApp = "https://unsplash.com"
        let activityActionBar = UIActivityViewController(activityItems: [shareApp], applicationActivities: [])
        activityActionBar.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
            present(activityActionBar, animated: true)
    }
    
    @IBAction private func dismissModalView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func shareApp(_ sender: Any) {
        if let url = URL(string: "https://unsplash.com/s/photos/unsplash-app") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction private func openVisitUrl(_ sender: Any) {
        openUrl(url: "https://unsplash.com")
    }
    
    @IBAction private func openLicense(_ sender: Any) {
        openUrl(url: "https://unsplash.com/license")
    }
}
