//
//  PhotoTechSpecificationsViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/14/21.
//

import UIKit

class PhotoTechSpecificationsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - IBActions
    @IBAction private func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
