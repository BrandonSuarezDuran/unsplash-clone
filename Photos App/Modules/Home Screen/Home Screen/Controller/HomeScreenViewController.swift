//
//  FirstScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/5/21.
//

import UIKit

class HomeScreenViewController: UIViewController, HomeScreenDelegate {

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.backgroundColor = nil
        }
    }
    @IBOutlet private weak var separatorView: UIView! {
        didSet {
            self.separatorView.layer.borderColor = CGColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    // MARK: - Constants
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    // MARK: - Variables
    var selectedCell: [Bool] = []
    lazy var viewModel = HomeScreenViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchPhotoData() // With this i populate the datasource for the first table view
        viewModel.fetchTopics() // The collection View
        viewModel.mergeData(with: .main)
        selectedCell = viewModel.getTopicsActivateCellArray()
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func reloadCollection() {
        collectionView.reloadData()
    }
}

// MARK: - Table View - > Delegate
extension HomeScreenViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let photoUrl = viewModel.getDataPhotoUrl(at: index)
        let name = viewModel.getDataName(at: index)
        let userID = viewModel.getDataID(at: index)
        let blurHash = viewModel.getBlurHash(at: index)
        
        guard let viewController = storyboard?.instantiateViewController(identifier: "ImageScreenViewController") as? PhotoScreenViewController else {
            fatalError("Not Passed ImageScreenViewController")
        }
        viewController.modalPresentationStyle = .fullScreen
        viewController.photoUrl = photoUrl
        viewController.name = name
        viewController.userID = userID
        viewController.blurHash = blurHash
        self.present(viewController, animated: true, completion: nil)
    }
}
// MARK: - Table View -> Data Source
extension HomeScreenViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getDataNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let index = indexPath.row
        guard let firstPhotoCell = tableView.dequeueReusableCell(withIdentifier: "FirstPictureTableViewCell") as? PhotoOfTheDayTableViewCell else {
            fatalError("FirstPhoto Cell not dequeued")
        }
        guard let nonSponsoredCell = tableView.dequeueReusableCell(withIdentifier: "NonSponsoredTableViewCell") as? PhotoTableViewCell else {
            fatalError("NonSponsored Cell not dequeued")
        }
        if index == 0 {
            firstPhotoCell.configure(name: viewModel.getDataName(at: index), photoUrl: viewModel.getDataPhotoUrl(at: index))
            cell = firstPhotoCell
        } else {
            nonSponsoredCell.configure(name: viewModel.getDataName(at: index), photoUrl: viewModel.getDataPhotoUrl(at: index))
            cell = nonSponsoredCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = indexPath.row
        let height = viewModel.getDataPhotoHeight(at: index)
        return height
    }
}
// MARK: - Collection View - > Delegate
extension HomeScreenViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let topic = (viewModel.getTopicsTitle(at: index)).lowercased()
        viewModel.fetchTopicsPhotos(with: topic)
        if index != 0 {
            viewModel.mergeData(with: .topic)
        } else {
            viewModel.mergeData(with: .main)
        }
    }
}
// MARK: - Collection View -> DataSource
extension HomeScreenViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.getTopicsNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.item
        let topic = viewModel.getTopicsTitle(at: index)
        var collection = UICollectionViewCell()
        guard let categoriesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as? CategoriesCollectionViewCell
        else {
            fatalError("Categories Collection View not Dequeued")
        }
        if index == 0 {
            categoriesCell.configure(topic: "Editorial")
            collection = categoriesCell
            return collection
        }
        categoriesCell.configure(topic: topic)
        collection = categoriesCell
        return collection
    }
}
