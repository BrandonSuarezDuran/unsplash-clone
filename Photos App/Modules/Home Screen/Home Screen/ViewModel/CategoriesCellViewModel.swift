//
//  CategoriesCellViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/13/21.
//

import Foundation
protocol CategoriesCellViewModelDelegate: AnyObject {
    func reloadCollection()
}

class CategoriesCellViewModel {
    private let routerInstance = Router<EndPoint>()
    // MARK: - Data Source
    private var dataSource: [ListTopics]
    weak var delegate: CategoriesCellViewModelDelegate?
    private var slug: [String] {
        didSet {
            self.delegate?.reloadCollection()
        }
    }
    
    // MARK: Init
    init() {
        self.dataSource = []
        self.slug = []
    }
    // MARK: - Functions
    func fetchTopicsData() {
        routerInstance.request(.listTopics) { (result: Result<[ListTopics], AppError>) in
            switch result {
            case .success(let data):
                self.dataSource = data
                for element in self.dataSource {
                    if let topic = element.slug {
                        self.slug.append(topic)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }

    func getCollectionCount() -> Int {
        slug.count
    }
    
    func getTopics(at index: Int) -> String {
        self.slug[index]
    }
}
