//
//  photoCellViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/11/21.
//

import Foundation
import UIKit

class PhotoCellViewModel {
    // MARK: - Router
    private let routerInstance = Router<EndPoint>()
    // MARK: - Data Source
    private var dataSource: [ListCollection]
    // MARK: - Author Name
    var authorName: [String] {
        var rawData: [String] = []
        for element in self.dataSource {
            if let userName = element.user?.name {
                rawData.append(userName)
            }
        }
        return rawData
    }
    // MARK: - Photo
    var photoUrl: [String] { // Create an image cache
        var rawData: [String] = []
        for element in self.dataSource {
            if let photoUrl = element.coverPhoto?.urls?.small {
                rawData.append(photoUrl)
            }
        }
        return rawData
    }
    // MARK: - User Id
    var userIDData: [String] {
        var rawData: [String] = []
        for element in self.dataSource {
            if let ID = element.user?.id {
                rawData.append(ID)
            }
        }
        return rawData
    }
    // MARK: - Author Name
    private var authorFirstName: [String] {
        var rawData: [String] = []
        for element in dataSource {
            if let firstName = element.user?.firstName {
                rawData.append(firstName)
            }
        }
        return rawData
    }
    // MARK: - Author Last Name
    private var authorLastName: [String] {
        var rawData = [String]()
        for element in dataSource {
            if let lastName = element.user?.lastName {
                rawData.append(lastName)
            }
        }
        return rawData
    }
    // MARK: - Author Full Name
    var authorFullName: [String] {
        var rawData: [String] = []
        for index in 0..<dataSource.count {
            let name = "\(authorFirstName[index]) \(authorLastName[index])"
            rawData.append(name)
        }
        return rawData
    }
    // MARK: - Photo Tech Details
    var photoDetails: [String] = []
    // MARK: - Init
    init() {
        self.dataSource = []
    }

    func fetchPhotoData() {
        routerInstance.request(.collections) { (result: Result<[ListCollection], AppError>) in
            switch result {
            case .success(let data):
                self.dataSource = data

            case .failure(let error):
                print(error)
            }
        }
    }

    func numberOfRows() -> Int {
        self.dataSource.count
    }
}
