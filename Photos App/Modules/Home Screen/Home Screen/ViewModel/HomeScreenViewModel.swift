//
//  FirstScreenViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/7/21.
//

import Foundation
import UIKit

protocol HomeScreenDelegate: AnyObject {
    func reloadTable()
    func reloadCollection()
}

enum TableDataSource: Int {
    case main = 0
    case topic = 1
}

class HomeScreenViewModel {
    // MARK: - Constants
    private let routerInstance = Router<EndPoint>()
    private let screenWidth = HomeScreenViewController().screenWidth
    // MARK: - Variables
    var dataSource: [ListCollection] // - >This is for the first table view
    private var topicsDataSource: [ListTopics]
    private var topicPhotosDataSource: [GetATopicsPhotos] // -> This is for the other topics
    
    private var globalDataSource: [DataSource] {
        didSet {
            self.delegate?.reloadTable()
        }
    }
    private var selectedCell: [Bool] = []
    weak var delegate: HomeScreenDelegate?
    // MARK: - Init
    init(delegate: HomeScreenDelegate) {
        self.dataSource = []
        self.topicPhotosDataSource = []
        self.topicsDataSource = []
        self.globalDataSource = []
        self.delegate = delegate
    }
    // MARK: - Global Data Source Function
    func mergeData(with source: TableDataSource) {
        switch source {
        case .main:
            globalDataSource = []
            for element in dataSource {
                guard let name = element.user?.name,
                      let photoUrl = element.coverPhoto?.urls?.small,
                      let userID = element.user?.id,
                      let width = element.coverPhoto?.width,
                      let height = element.coverPhoto?.height,
                      let blurHash = element.coverPhoto?.blurHash
                else {
                    return
                }
                let dataSourceInstance = DataSource(name: name, photoUrl: photoUrl, userID: userID, width: width, height: height, blurHash: blurHash)
                self.globalDataSource.append(dataSourceInstance)
            }
            
        case .topic:
            globalDataSource = []
            for element in topicPhotosDataSource {
                guard let name = element.user?.name,
                      let photoUrl = element.urls?.small,
                      let userID = element.id,
                      let width = element.width,
                      let height = element.height,
                      let blurHash = element.blurHash
                else {
                    return
                }
                let dataSourceInstance = DataSource(name: name, photoUrl: photoUrl, userID: userID, width: width, height: height, blurHash: blurHash)
                self.globalDataSource.append(dataSourceInstance)
            }
        }
    }
    
    func getDataNumberOfRows() -> Int {
        globalDataSource.count
    }
    
    func getDataName(at index: Int) -> String {
        globalDataSource[index].name
    }
    
    func getDataPhotoUrl(at index: Int) -> String {
        globalDataSource[index].photoUrl
    }
    
    func getDataID(at index: Int) -> String {
        globalDataSource[index].userID
    }
    
    func getBlurHash(at index: Int) -> String {
        globalDataSource[index].blurHash
    }
    
    func getDataPhotoHeight(at index: Int) -> CGFloat {
        let height = globalDataSource[index].height
        let width = globalDataSource[index].width
        let newHeight = (CGFloat(height)) * ((screenWidth) / CGFloat(width))
        return newHeight
    }
    
    // MARK: - Photos Functions
    func getNumberOfRows() -> Int {
        dataSource.count
    }
    
    func fetchPhotoData() {
        routerInstance.request(.collections) { [weak self] (result: Result<[ListCollection], AppError>) in
            switch result {
            case .success(let data):
                self?.dataSource = data
                self?.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPhotoUrl(at index: Int) -> String {
        guard let photoUrl = dataSource[index].coverPhoto?.urls?.small else {
            fatalError("Photo Url Not Found")
        }
        return photoUrl
    }
    
    func getPhotoName(at index: Int) -> String {
        guard let photoName = dataSource[index].user?.name else {
            fatalError("Photo Name not Found")
        }
        return photoName
    }
    
    func getPhotoUserID(at index: Int) -> String {
        guard let userID = dataSource[index].user?.id else {
            return ""
        }
        return userID
    }
    
    func getPhotoHeight(at index: Int) -> CGFloat {
        guard let height = dataSource[index].coverPhoto?.height,
              let width = dataSource[index].coverPhoto?.width
        else {
            print("Cell Height is the third of the screen size")
            return screenWidth / 3
        }
        let newHeight = (CGFloat(height)) * ((screenWidth) / CGFloat(width))
        return newHeight
    }
    
    // MARK: - Topic's Functions
    func fetchTopics() {
        routerInstance.request(.listTopics) { [weak self] (result: Result<[ListTopics], AppError>) in
            switch result {
            case .success(let data):
                self?.topicsDataSource = data
                self?.delegate?.reloadCollection()
            
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getTopicsNumberOfItems() -> Int {
        topicsDataSource.count
    }
    
    func getTopicsActivateCellArray() -> [Bool] {
        for _ in 0...topicsDataSource.count {
            selectedCell.append(false)
        }
        return selectedCell
    }
    
    func getTopicsTitle(at index: Int) -> String {
        guard let title = topicsDataSource[index].title else {
            return "Topic Title Not Fetched"
        }
        return title
    }
    
    func fetchTopicsPhotos(with topicID: String) {
        routerInstance.request(.getATopicsPhotos(id: topicID)) { [weak self] (result: Result<[GetATopicsPhotos], AppError>) in
            switch result {
            case .success(let data):
                self?.topicPhotosDataSource = data
                self?.delegate?.reloadTable()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getTopicsPhotosNumberOfRows() -> Int {
        topicPhotosDataSource.count
    }
    
    func getTopicsPhotos(at index: Int) -> String {
        guard let photoUrl = topicPhotosDataSource[index].urls?.small else {
            return "Topic's Photos Not Obtained"
        }
        return photoUrl
    }
    
    func getTopicsPhotosNames(at index: Int) -> String {
        guard let name = topicPhotosDataSource[index].user?.name else {
            return "Topic's Photos Names Not Obtained"
        }
        return name
    }
    
    func getTopicsPhotosID(at index: Int) -> String {
        guard let ID = topicPhotosDataSource[index].id else {
            return "Topic's Photos IDs Not Obtained"
        }
        return ID
    }
    
    func getTopicsPhotosHeight(at index: Int) -> CGFloat {
        guard let height = topicPhotosDataSource[index].height,
              let width = topicPhotosDataSource[index].width
        else {
            print("Cell Height is the third of the screen size")
            return screenWidth / 3
        }
        let newHeight = (CGFloat(height)) * ((screenWidth) / CGFloat(width))
        return newHeight
    }
}
