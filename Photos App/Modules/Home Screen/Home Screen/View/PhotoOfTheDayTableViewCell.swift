//
//  FirstPictureTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/26/21.
//

import UIKit

class PhotoOfTheDayTableViewCell: UITableViewCell {

    @IBOutlet private weak var photoImage: UIImageView!
    @IBOutlet private weak var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(name: String, photoUrl: String) {
        self.authorName.text = name
        self.photoImage.downloadImage(with: photoUrl)
    }
}
