//
//  NonSponsoredTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/10/21.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet private weak var photoImage: UIImageView!
    @IBOutlet private weak  var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(name: String, photoUrl: String) {
        self.authorName.text = name
        self.photoImage.downloadImage(with: photoUrl)
    }
}
