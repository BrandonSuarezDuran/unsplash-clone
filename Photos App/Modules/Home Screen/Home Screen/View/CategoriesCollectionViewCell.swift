//
//  CategoriesCollectionViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/12/21.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var categoryLabel: UILabel!
    @IBOutlet private weak var bottomView: UIView! {
        didSet {
            self.bottomView.layer.backgroundColor = CGColor(red: 1, green: 1, blue: 1, alpha: 0)
        }
    }
    
    func configure(topic: String) {
        self.categoryLabel.text = topic
    }
    
    func activateCell(activated: Bool) {
        if activated {
            self.bottomView.layer.backgroundColor = CGColor(red: 1, green: 1, blue: 11, alpha: 1)
        } else {
            self.bottomView.layer.backgroundColor = CGColor(red: 1, green: 1, blue: 11, alpha: 0)
        }
    }
}
