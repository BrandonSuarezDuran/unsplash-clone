//
//  Topics.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/12/21.
//

import Foundation
import UIKit

struct ListTopics: Decodable {
    var id: String?
    var slug: String?
    var title: String?
    var description: String?
    var publishedAt: String?
    var updatedAt: String?
    var startsAt: String?
//    var endsAt: String?
    var featured: Bool
    var totalPhotos: Int?
    var currentUserContributions: [Int]?
//    var totalCurrentUserSubmissions: Int?
    var links: TopicsLinks?
    var status: String?
    var owners: [User]?
    var coverPhoto: CoverPhoto?
    var previewPhotos: [PreviewPhotos]
    
    enum CodingKeys: String, CodingKey {
        case id, slug, title, description, featured, links, status, owners
        case publishedAt = "published_at"
        case updatedAt = "updated_at"
        case startsAt = "starts_at"
//        case endsAt = "ends_at"
        case totalPhotos = "total_photos"
        case currentUserContributions = "current_user_contributions"
        case coverPhoto = "cover_photo"
        case previewPhotos = "preview_photos"
    }
}

struct TopicsLinks: Decodable {
    var itSelf: String
    var html: String
    var photos: String
    
    enum CodingKeys: String, CodingKey {
        case html, photos
        case itSelf = "self"
    }
}
