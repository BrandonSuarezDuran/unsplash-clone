//
//  DataSource.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/10/21.
//

import Foundation
import UIKit

class DataSource {
    var name: String
    var photoUrl: String
    var userID: String
    var width: Int
    var height: Int
    var blurHash: String
    
    init(name: String, photoUrl: String, userID: String, width: Int, height: Int, blurHash: String) {
        self.name = name
        self.photoUrl = photoUrl
        self.userID = userID
        self.width = width
        self.height = height
        self.blurHash = blurHash
    }
}
