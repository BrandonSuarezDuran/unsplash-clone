//
//  Photo.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/7/21.
//

import Foundation
import UIKit

struct ListCollection: Decodable {
    var id: String?
    var title: String?
    var description: String?
    var publishedAt: String?
    var lastCollectedAt: String?
    var updatedAt: String?
    var curated: Bool
    var featured: Bool
    var totalPhotos: Int?
    var privateBool: Bool
    var shareKey: String?
    var tags: [Tag]?
    var links: Links?
    var user: User?
    var coverPhoto: CoverPhoto?
    var previewPhotos: [PreviewPhotos]?

    enum CodingKeys: String, CodingKey {
        case id, title, description, curated, featured, tags, links, user
        case publishedAt = "published_at"
        case lastCollectedAt = "last_collected_at"
        case updatedAt = "updated_at"
        case totalPhotos = "total_photos"
        case privateBool = "private"
        case shareKey = "share_key"
        case coverPhoto = "cover_photo"
        case previewPhotos = "preview_photos"
    }
}

struct Tag: Decodable {
    var type: String?
    var title: String?
//    var source: [Source]?
}

struct Source: Decodable {
    var ancestry: [Ancestry]?
    var title: String?
    var subtitle: String?
    var description: String?
    var metaTitle: String?
    var metaDescription: String?
    var coverPhoto: CoverPhoto?
    
    enum CodingKeys: String, CodingKey {
        case ancestry, title, subtitle, description
        case metaTitle = "meta_title"
        case metaDescription = "meta_description"
    }
}

struct Ancestry: Decodable {
    var type: [Type]?
    var category: [Type]?
}

struct Type: Decodable {
    var slug: String?
    var prettySlug: String?
    
    enum CodingKeys: String, CodingKey {
        case slug
        case prettySlug = "pretty_slug"
    }
}

struct Links: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var related: String?

    enum CodingKeys: String, CodingKey {
        case html, photos, related
        case itSelf = "self"
    }
}

struct User: Decodable {
    var id: String?
    var updatedAt: String?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var twitterUsername: String?
    var portfolioUrl: String?
    var bio: String?
    var location: String?
    var links: UserLinks?
    var profileImage: ProfileImage?
    var instagramUserName: String?
    var totalCollections: Int?
    var totalLikes: Int?
    var totalPhotos: Int?
    var acceptedTos: Bool
    var forHire: Bool

    enum CodingKeys: String, CodingKey {
        case id, username, name, bio, location, links
        case updatedAt = "updated_at"
        case firstName = "first_name"
        case lastName = "last_name"
        case twitterUsername = "twitter_username"
        case portfolioUrl = "portfolio_url"
        case profileImage = "profile_image"
        case instagramUserName = "instagram_username"
        case totalCollections = "total_collections"
        case totalLikes = "total_likes"
        case totalPhotos = "total_photos"
        case acceptedTos = "accepted_tos"
        case forHire = "for_hire"
    }
}

struct UserLinks: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    var following: String?
    var followers: String?

    enum CodingKeys: String, CodingKey {
        case html, photos, likes, portfolio, following, followers
        case itSelf = "self"
    }
}

struct ProfileImage: Decodable {
    var small: String?
    var medium: String?
    var large: String?
}

struct CoverPhoto: Decodable {
    var id: String?
    var createdAt: String?
    var updatedAt: String?
    var promotedAt: String?
    var width: Int?
    var height: Int?
    var color: String?
    var blurHash: String?
    var description: String?
    var altDescription: String?
    var urls: Urls?  // --> here it is
    var links: CoverPhotoLinks?
    var categories: [String]
    var likes: Int?
    var likedByUser: Bool
    var currentUserCollections: [Int]
//    var sponsorship: Bool
    var user: User?

    enum CodingKeys: String, CodingKey {
        case id, width, height, color, description, urls, links, categories, likes, user
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case promotedAt = "promoted_at"
        case blurHash = "blur_hash"
        case altDescription = "alt_description"
        case likedByUser = "liked_by_user"
        case currentUserCollections = "current_user_collections"
    }
}

struct Urls: Decodable {
    var raw: String?
    var full: String?
    var regular: String?
    var small: String?
    var thumb: String?
}

struct CoverPhotoLinks: Decodable {
    var itSelf: String?
    var html: String?
    var download: String?
    var downloadLocation: String?

    enum CodingKeys: String, CodingKey {
        case html, download
        case itSelf = "self"
        case downloadLocation = "download_location"
    }
}

struct PreviewPhotos: Decodable {
    var id: String?
    var createdAt: String?
    var updatedAt: String?
    var blurHash: String?
    var urls: Urls?

    enum CodingKeys: String, CodingKey {
        case id, urls
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case blurHash = "blur_hash"
    }
}
