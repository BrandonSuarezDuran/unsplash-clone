//
//  GetATopicsPhotos.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/8/21.
//

import Foundation
import UIKit

struct GetATopicsPhotos: Decodable {
    var id: String?
    var width: Int?
    var height: Int?
    var blurHash: String?
    var user: User?
    var urls: Urls?
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, user, urls
        case blurHash = "blur_hash"
    }
}
