//
//  PhotoScreenViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/14/21.
//

import Foundation
import UIKit

class PhotoScreenViewModel {
    // MARK: - Constants
    let routerInstance = Router<EndPoint>()
    // MARK: - Variables
    private var dataSource: [GetAPhoto]
    var exifData: [Exif] {
        var rawData: [Exif] = []
        for element in self.dataSource {
            if let exif = element.exif {
                rawData.append(exif)
            }
        }
        return rawData
    }
    // MARK: - Init
    init() {
        self.dataSource = []
    }
    // MARK: - Functions
    func fetchGetAPhotoData() {
        routerInstance.request(.photos) { (result: Result<[GetAPhoto], AppError>) in
            switch result {
            case .success(let data):
                print(data)
                self.dataSource = data
                
            case .failure(let error):
                print("--------------Get a Photo Photo Error Print Starts--------------")
                print(error)
                print("---------------Get a Photo Photo Error Print Ends----------------")
            }
        }
    }
}
