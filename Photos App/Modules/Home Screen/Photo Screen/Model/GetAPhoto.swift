//
//  GetAPhoto.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/14/21.
//

import Foundation
import UIKit

struct GetAPhoto: Decodable {
    var id: String?
    var createdAt: String?
    var updatedAt: String?
    var width: String?
    var height: String?
    var color: String?
    var blurHash: String?
    var downloads: Int?
    var likes: Int?
    var likedByUser: Bool
    var description: String?
    var exif: Exif?
    var location: Location
    var tags: [Tag]?
    var currentUserLocation: [CurrentUserCollection]?
    var urls: Urls?
    var getAPhotoLinks: GetAPhotoLinks?
    var user: GetAPhotoUser?
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, color, downloads, likes, description, exif, location, tags, urls, user
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case blurHash = "blur_hash"
        case likedByUser = "liked_by_user"
        case currentUserLocation = "current_user_collections"
        case getAPhotoLinks = "links"
    }
}

struct Exif: Decodable {
    var make: String?
    var mode: String?
    var exposureTime: String?
    var aperture: String?
    var focalLength: String?
    var iso: Int?
    
    enum CodingKeys: String, CodingKey {
        case make, mode, aperture, iso
        case exposureTime = "exposure_time"
        case focalLength = "focal_length"
    }
}
struct Location: Decodable {
    var city: String?
    var country: String?
    var position: Position?
}

struct Position: Decodable {
    var latitude: Double
    var longitude: Double
}

struct CurrentUserCollection: Decodable {
    var id: Int?
    var title: String?
    var publishedAt: String?
    var lastCollectedAt: String?
    var updateAt: String?
//    var covePhoto: String?
//    var user: User?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case publishedAt = "published_at"
        case lastCollectedAt = "last_collected_at"
        case updateAt = "update_at"
//        case covePhoto = "cove_photo"
    }
}

struct GetAPhotoLinks: Decodable {
    var itSelf: String?
    var html: String?
    var download: String?
    var downloadLocation: String?
    
    enum CodingKeys: String, CodingKey {
        case html, download
        case itSelf = "self"
        case downloadLocation = "download_location"
    }
}

struct GetAPhotoUser: Decodable {
    var id: String?
    var updatedAt: String?
    var username: String?
    var name: String?
    var portfolioUrl: String?
    var bio: String?
    var location: String?
    var totalLikes: Int?
    var totalPhotos: Int?
    var totalCollections: Int?
    var links: GetAPhotoUserLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, bio, location, links
        case updatedAt = "updated_at"
        case portfolioUrl = "portfolio_url"
        case totalLikes = "total_likes"
        case totalPhotos = "total_photos"
        case totalCollections = "total_collections"
    }
}

struct GetAPhotoUserLinks: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    
    enum CodingKeys: String, CodingKey {
        case html, photos, likes, portfolio
        case itSelf = "self"
    }
}
