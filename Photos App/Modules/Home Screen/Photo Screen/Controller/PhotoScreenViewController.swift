//
//  PhotoScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/13/21.
//

import UIKit

class PhotoScreenViewController: UIViewController {
        
    // MARK: - Outlets
    @IBOutlet private weak var likeButton: UIButton! {
        didSet {
            self.likeButton.layer.masksToBounds = true
            self.likeButton.layer.cornerRadius = self.likeButton.frame.height / 2.0
        }
    }
    
    @IBOutlet private weak var addButton: UIButton! {
        didSet {
            self.addButton.layer.masksToBounds = true
            self.addButton.layer.cornerRadius = self.addButton.frame.height / 2.0
        }
    }
    
    @IBOutlet private weak var downloadButton: UIButton! {
        didSet {
            self.downloadButton.layer.masksToBounds = true
            self.downloadButton.layer.cornerRadius = self.downloadButton.frame.height / 2.0
        }
    }
    
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var authorName: UIButton!
    // MARK: - Variables
    var photoUrl: String?
    var name: String?
    var userID: String?
    var exifData: Exif?
    var blurHash: String?
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = photoUrl else {
            return image.image = UIImage(named: "heart.card.svg")
        }
        image.downloadImage(with: url)
        guard let name = name else {
            return authorName.setTitle("Author Name", for: .normal)
        }
        authorName.setTitle(name, for: .normal)
    }
    // MARK: - IBActions
    @IBAction private func dismissModalView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func sharePhoto(_ sender: Any) { // Fix it to share the the photo.
        let shareApp = "https://unsplash.com"
        let activityActionBar = UIActivityViewController(activityItems: [shareApp], applicationActivities: [])
        activityActionBar.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
            present(activityActionBar, animated: true)
    }
    @IBAction private func openAuthorScreen(_ sender: Any) {
        guard let name = self.name else {
            return
        }
        guard let viewController = storyboard?.instantiateViewController(identifier: "AuthorScreenViewController") as? AuthorScreenViewController else {
            fatalError(" Author Screen Not Opened")
        }
        viewController.name = name
        if let blurHash = self.blurHash {
            viewController.blurHash = blurHash
        }
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction private func openPhotoInfo(_ sender: Any) {
        guard let viewController = storyboard?.instantiateViewController(identifier: "PhotoTechSpecificationsViewController") as? PhotoTechSpecificationsViewController else {
            fatalError("Photo Specifications Screen Not passed")
        }
        viewController.modalPresentationStyle = .formSheet
        self.present(viewController, animated: true, completion: nil)
    }
}
