//
//  UploadImageViewModel.swift
//  Photos App
//
//  Created by Brandon Suarez on 6/22/21.
//

import Foundation

protocol UploadImageDelegate: AnyObject {
    func reloadCollection()
}

class UploadImageViewModel {
    
    private let  routerInstance = Router<EndPoint>()
    private var dataSource: [ListCollection] {
        didSet {
            self.delegate?.reloadCollection()
        }
    }
    
    weak var delegate: UploadImageDelegate?
    
    init(delegate: UploadImageDelegate) {
        self.dataSource = []
        self.delegate = delegate
    }
    
    func fetchData() {
        routerInstance.request(.listTopics) { [weak self] (result: Result<[ListCollection], AppError>) in
            switch result {
            case .success(let data):
                self?.dataSource = data
                self?.delegate?.reloadCollection()
                
            case .failure(let error):
                print("Upload Image View Model's Error: ")
                print(error)
            }
        }
    }
    
    func getCollectionPhoto(at index: Int) -> String {
        guard let photo = dataSource[index].coverPhoto?.urls?.small else {
            return ""
        }
        return photo
    }
    
    func  getCollectionName(at index: Int) -> String {
        guard let name = dataSource[index].user?.name else {
            return ""
        }
        return name
    }
    
    func getCollectionContributionName(at index: Int) -> String {
        guard let title = dataSource[index].coverPhoto?.description else {
            return ""
        }
        return title
    }
    
    func getCollectionLink(at index: Int) -> String {
        guard let link = dataSource[index].user?.links?.portfolio else {
            return ""
        }
        return link
    }
    
    func getNumberOfItems() -> Int {
        dataSource.count
    }
}
