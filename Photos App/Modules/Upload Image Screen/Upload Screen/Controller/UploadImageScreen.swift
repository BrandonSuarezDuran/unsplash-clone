//
//  ThirdScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import UIKit

class UploadImageScreen: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    // MARK: - Variables
    lazy var viewModel = UploadImageViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
    }
}
    // MARK: - Functions
extension UploadImageScreen: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        if let url = URL(string: viewModel.getCollectionLink(at: index)) {
            UIApplication.shared.open(url)
        } else {
            print("No Url Assigned")
        }
    }
}

extension UploadImageScreen: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.getNumberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.item
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlogCollectionViewCell", for: indexPath) as? AccessToBlogCollectionViewCell else {
            fatalError("Third Screen Blog Collection View Cell Not Dequeued")
        }
        cell.configure(
            imageUrl: viewModel.getCollectionPhoto(at: index),
            contribution: viewModel.getCollectionContributionName(at: index),
            name: viewModel.getCollectionName(at: index)
        )
        
        return cell
    }
}

extension UploadImageScreen: UploadImageDelegate {
    func reloadCollection() {
        collectionView.reloadData()
    }
}
