//
//  BlogCollectionViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import UIKit

class AccessToBlogCollectionViewCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var blogImage: UIImageView!
    @IBOutlet private weak var contributionNameLabel: UILabel!
    @IBOutlet private weak var authorNameLabel: UILabel!
    
    // MARK: - Functions
    func configure(imageUrl: String, contribution: String, name: String) {
        self.blogImage.downloadImage(with: imageUrl)
        self.contributionNameLabel.text = contribution
        self.authorNameLabel.text = name
    }
}
