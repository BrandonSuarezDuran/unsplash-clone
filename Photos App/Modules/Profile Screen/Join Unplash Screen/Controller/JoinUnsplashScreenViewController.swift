//
//  JoinUnsplashViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/19/21.
//

import UIKit

class JoinUnsplashScreenViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var signUpButton: UIButton! {
        didSet {
            self.signUpButton.layer.masksToBounds = true
            self.signUpButton.layer.cornerRadius = self.signUpButton.frame.height / 20.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - Functions
    @IBAction private func goBackButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
