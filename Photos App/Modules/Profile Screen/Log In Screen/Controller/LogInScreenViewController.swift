//
//  FourthScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import UIKit

class LogInScreenViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var userTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginButton: UIButton! {
        didSet {
            self.loginButton.layer.masksToBounds = true
            self.loginButton.layer.cornerRadius = self.loginButton.frame.height / 20.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction private func logInButtonTapped(_ sender: Any) {
    }
    
    @IBAction private func forgotPasswordButtonTapped(_ sender: Any) {
    }
    
    @IBAction private func joinButtonTapped(_ sender: Any) {
    }
}
