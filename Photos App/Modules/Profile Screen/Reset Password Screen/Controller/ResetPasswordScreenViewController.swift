//
//  ResetPasswordViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/19/21.
//

import UIKit

class ResetPasswordScreenViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var resetPasswordButton: UIButton! {
        didSet {
            self.resetPasswordButton.layer.masksToBounds = true
            self.resetPasswordButton.layer.cornerRadius = self.resetPasswordButton.frame.height / 20.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - Functions
    @IBAction private func goBackButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
