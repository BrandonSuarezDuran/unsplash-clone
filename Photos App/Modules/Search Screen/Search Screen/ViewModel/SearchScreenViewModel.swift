//
//  SecondScreenViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import Foundation
import UIKit

protocol SearchDelegate: AnyObject {
    func reload()
}

class SearchScreenViewModel {
    // MARK: - Constants
    let routerInstance: Router<EndPoint> = Router<EndPoint>()
    // MARK: - Data Sources
    private var dataSource: [ListTopics]
    // MARK: - Fetch Photos DataSource
    private var photosDataSource: [PhotoResults]
    // MARK: - Fetch Collections DataSource
    private var collectionsDataSource: [CollectionResults]
    // MARK: - Fetch Users DataSource
    private var usersDataSource: [UserResults]
    // MARK: - Delegate
    weak var delegate: SearchDelegate?
    // MARK: - Init
    init(delegate: SearchDelegate) {
        self.dataSource = []
        self.photosDataSource = []
        self.collectionsDataSource = []
        self.usersDataSource = []
        self.delegate = delegate
    }
    // MARK: - Functions
    func fetchTopics() { // First Call this function
        routerInstance.request(.listTopics) { (result: Result<[ListTopics], AppError>) in
            switch result {
            case .success(let data):
                self.dataSource = data
                self.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK: - Number of Rows Functions to Table View
    func getNumberOfRowsOnTopics() -> Int {
        5
    }
    // MARK: - Search Photo
    func searchPhotos(searchQuery: String) {
        routerInstance.request(.searchPhoto((query: searchQuery, page: "1"))) { [weak self] (result: Result< SearchPhotos, AppError>) in
            switch result {
            case .success(let data):
                self?.photosDataSource = data.results
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }

    func getSearchPhotoUrl(at index: Int) -> String {
        guard let photoUrl = photosDataSource[index].urls?.small else {
            fatalError("No Photo Url Found")
        }
        return photoUrl
    }
    
    func getSearchPhotoName(at index: Int) -> String {
        guard let authorName = photosDataSource[index].user?.name else {
            fatalError("No Photo User Name Found")
        }
        return authorName
    }
    
    func getNumberOfRowsSearchPhotos() -> Int {
        self.photosDataSource.count
    }
    
    func searchCollections(searchQuery: String) {
        routerInstance.request(.searchCollection((query: searchQuery, page: "1"))) {[weak self] (result: Result< SearchCollections, AppError>) in
            switch result {
            case .success(let data):
                self?.collectionsDataSource = data.results
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getSearchCollectionPhotoUrl(at index: Int) -> String {
        guard let photoUrl = collectionsDataSource[index].coverPhoto.urls?.small else {
            fatalError("No Collection Cover Url Found")
        }
        return photoUrl
    }
    
    func getSearchCollectionName(at index: Int) -> String {
        guard let collectionName = collectionsDataSource[index].title else {
            fatalError("No Collection Name Found")
        }
        return collectionName
    }
    
    func getSearchCollectionId(at index: Int) -> String {
        guard let id = collectionsDataSource[index].id else {
            fatalError("No Collection ID Found")
        }
        return id
    }
    
    func getNumberOfRowsSearchCollections() -> Int {
        self.collectionsDataSource.count
    }
    
    func searchUsers(searchQuery: String) {
        routerInstance.request(.searchUser((query: searchQuery, page: "1"))) { (result: Result<SearchUsers, AppError>) in
            switch result {
            case .success(let data):
                self.usersDataSource = data.results
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getSearchUserProfilePhotoUrl(at index: Int) -> String {
        guard let photoUrl = usersDataSource[index].profileImage?.small else {
            fatalError("No User Profile Image Url Found")
        }
        return photoUrl
    }
    
    func getSearchUserUserName(at index: Int) -> String {
        guard let userName = usersDataSource[index].username else {
            fatalError("No User Name Found")
        }
        return userName
    }
    
    func getSearchUserName(at index: Int) -> String {
        guard let name = usersDataSource[index].name else {
            fatalError("No Name Found")
        }
        return name
    }
    
    func getNumberOfRowsSearchUsers() -> Int {
        self.usersDataSource.count
    }
}
