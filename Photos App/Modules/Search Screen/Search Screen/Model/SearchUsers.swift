//
//  SearchUsers.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/26/21.
//

import Foundation

struct SearchUsers: Decodable {
    var total: Int?
    var totalPages: Int?
    var results: [UserResults]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}

struct UserResults: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var instagramUsername: String?
    var twitterUsername: String?
//    var portfolioUrl: String?
    var totalLikes: Int?
    var totalPhotos: Int?
    var totalCollections: Int?
    var profileImage: ProfileImage?
    var links: SearchPhotoLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, links
        case firstName = "first_name"
        case lastName = "last_name"
        case instagramUsername = "instagram_username"
        case twitterUsername = "twitter_username"
        case totalLikes = "total_likes"
        case totalPhotos = "total_photos"
        case totalCollections = "total_collections"
        case profileImage = "profile_image"
    }
}
