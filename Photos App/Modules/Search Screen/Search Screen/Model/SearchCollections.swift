//
//  SearchCollections.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/26/21.
//

import Foundation

struct SearchCollections: Decodable {
    var total: Int?
    var totalPages: Int?
    var results: [CollectionResults]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}

struct CollectionResults: Decodable {
    var id: String?
    var title: String?
//    var description: String?
    var publishedAt: String?
    var lastCollectedAt: String?
    var updatedAt: String?
    var featured: Bool
    var totalPhotos: Int?
    var collectionPrivate: Bool
    var shareKey: String?
    var coverPhoto: CollectionCoverPhoto
    var user: CollectionResultUser?
    var links: Links?
    
    enum CodingKeys: String, CodingKey {
        case id, title, featured, totalPhotos, user, links
        case publishedAt = "published_at"
        case lastCollectedAt = "last_collected_at"
        case updatedAt = "updated_at"
        case collectionPrivate = "private"
        case shareKey = "share_key"
        case coverPhoto = "cover_photo"
    }
}

struct CollectionCoverPhoto: Decodable {
    var id: String?
    var createdAt: String?
    var width: Int?
    var height: Int?
    var color: String?
    var blurHash: String?
    var likes: Int?
    var likedByUser: Bool
    var description: String?
    var user: CollectionUser?
    var urls: Urls?
    var links: CollectionCoverPhotoLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, color, likes, description, user, urls, links
        case createdAt = "created_at"
        case blurHash = "blur_hash"
        case likedByUser = "liked_by_user"
    }
}

struct CollectionUser: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var instagramUsername: String?
    var twitterUsername: String?
    var portfolioUrl: String?
    var profileImage: ProfileImage
    var links: CollectionLinks
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, links
        case firstName = "first_name"
        case lastName = "last_name"
        case instagramUsername = "instagram_username"
        case twitterUsername = "twitter_username"
        case portfolioUrl = "portfolio_url"
        case profileImage = "profile_image"
    }
}

struct CollectionLinks: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    
    enum CodingKeys: String, CodingKey {
        case html, photos, likes
        case itSelf = "self"
    }
}

struct CollectionCoverPhotoLinks: Decodable {
    var itSelf: String?
    var html: String?
    var download: String?
    
    enum CodingKeys: String, CodingKey {
        case html, download
        case itSelf = "self"
    }
}

struct CollectionResultUser: Decodable {
    var id: String?
    var username: String?
    var name: String?
//    var portfolioUrl: String
    var bio: String?
    var profileImage: ProfileImage?
    var links: SearchPhotoLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, bio
        case profileImage = "profile_image"
    }
}
