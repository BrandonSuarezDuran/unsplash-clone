//
//  SearchPhoto.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/20/21.
//

import Foundation

struct SearchPhotos: Decodable {
    var total: Int?
    var totalPages: Int?
    var results: [PhotoResults]
    
    enum CodingKeys: String, CodingKey {
        case total, results
        case totalPages = "total_pages"
    }
}

struct PhotoResults: Decodable {
    var id: String?
    var cratedAt: String?
    var width: Int?
    var height: Int?
    var color: String?
    var blurHash: String?
    var likes: Int?
    var likedByUser: Bool
    var description: String?
    var user: SearchPhotoUser?
//    var currentUserCollection: [String]
    var urls: Urls?
    var links: ResultLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, color, likes, description, urls, links, user
        case cratedAt = "created_at"
        case blurHash = "blur_hash"
        case likedByUser = "liked_by_user"
//        case currentUserCollection = "current_user_collection"
    }
}

struct SearchPhotoUser: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var instagramUserName: String?
    var twitterUserName: String?
    var portfolioUrl: String?
    var profileImage: ProfileImage?
    var links: SearchPhotoLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, links
        case firstName = "first_name"
        case lastName = "last_name"
        case instagramUserName = "instagram_user_name"
        case twitterUserName = "twitter_username"
        case portfolioUrl = "portfolio_url"
        case profileImage = "profile_image"
    }
}

struct SearchPhotoLinks: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    
    enum CodingKeys: String, CodingKey {
        case html, photos, likes
        case itSelf = "self"
    }
}

struct ResultLinks: Decodable {
    var itSelf: String?
    var html: String?
    var download: String?
    
    enum CodingKeys: String, CodingKey {
        case html, download
        case itSelf = "self"
    }
}
