//
//  SecondScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import UIKit

class SearchScreenViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.isTranslucent = true
            self.searchBar.searchBarStyle = .minimal
            self.searchBar.delegate = self
        }
    }
    
    @IBOutlet private weak var segmentedControl: UISegmentedControl! {
        didSet {
            self.segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.normal)
            self.segmentedControl.selectedSegmentTintColor = UIColor(cgColor: CGColor(red: 100 / 255, green: 99 / 255, blue: 104 / 255, alpha: 1))
            self.segmentedControl.backgroundColor = UIColor(cgColor: CGColor(red: 41 / 255, green: 41 / 255, blue: 44 / 255, alpha: 1))
        }
    }
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.reloadData()
            self.tableView.register(UINib(nibName: "SearchCollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCollectionTableViewCell")
            self.tableView.register(UINib(nibName: "SearchUserTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchUserTableViewCell")
            self.tableView.register(UINib(nibName: "SearchPhotoTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchPhotoTableViewCell")
        }
    }
    
    // MARK: Variables
    private var searchQuery: String = ""
    // MARK: - View Model
    lazy var viewModel = SearchScreenViewModel(delegate: self)
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchTopics()
        let textInSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textInSearchBar?.textColor = UIColor.white
    }
    
    // MARK: - IBActions
    @IBAction private func didChangeSegment(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch index {
        case 0:
            self.reload()
            
        case 1:
            tableView.reloadData()
            
        case 2:
            tableView.reloadData()
            
        default:
            self.reload()
            tableView.reloadData()
        }
    }
}
    // MARK: - Table View Delegate
extension SearchScreenViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let segmentedControlIndex = segmentedControl.selectedSegmentIndex
        switch segmentedControlIndex {
        case 0:
            let name = viewModel.getSearchPhotoName(at: index)
            let photoUrl = viewModel.getSearchPhotoUrl(at: index)
            guard let viewController = storyboard?.instantiateViewController(identifier: "ImageScreenViewController") as? PhotoScreenViewController else {
                fatalError("Photo Screen Not Loaded")
            }
            viewController.modalPresentationStyle = .fullScreen
            viewController.photoUrl = photoUrl
            viewController.name = name
            self.present(viewController, animated: true, completion: nil)
            
        case 1:
            let name = viewModel.getSearchCollectionName(at: index)
            let id = viewModel.getSearchCollectionId(at: index)
            guard let viewController = storyboard?.instantiateViewController(identifier: "CollectionScreenViewController") as? CollectionPhotosScreenViewController else {
                fatalError("Collection Screen Not Loaded")
            }
            viewController.modalPresentationStyle = .fullScreen
            viewController.collectionName = name
            viewController.collectionID = id
            self.present(viewController, animated: true, completion: nil)
            
        case 2:
            let index = indexPath.row
            let name = viewModel.getSearchUserName(at: index)
            let profilePicture = viewModel.getSearchPhotoUrl(at: index)
            guard let viewController = storyboard?.instantiateViewController(identifier: "AuthorScreenViewController") as? AuthorScreenViewController else {
                fatalError("Author Screen Not Loaded")
            }
            viewController.modalPresentationStyle = .fullScreen
            viewController.name = name
            viewController.profilePictureUrl = profilePicture
            self.present(viewController, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

    // MARK: - Table View Data Source
extension SearchScreenViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let index = segmentedControl.selectedSegmentIndex
        switch index {
        case 0:
            return viewModel.getNumberOfRowsSearchPhotos()
            
        case 1:
            return viewModel.getNumberOfRowsSearchCollections()
            
        case 2:
            return viewModel.getNumberOfRowsSearchUsers()
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = segmentedControl.selectedSegmentIndex
        var cell = UITableViewCell()
        switch index {
        case 0:
            guard let photoCell = tableView.dequeueReusableCell(withIdentifier: "SearchPhotoTableViewCell", for: indexPath) as? SearchPhotoTableViewCell else {
                fatalError("Photo Search Cell not dequeued")
            }
            photoCell.configure(photoUrl: viewModel.getSearchPhotoUrl(at: indexPath.row), name: viewModel.getSearchPhotoName(at: indexPath.row))
            cell = photoCell
            
        case 1:
            guard let collectionCell = tableView.dequeueReusableCell(withIdentifier: "SearchCollectionTableViewCell", for: indexPath) as? SearchCollectionTableViewCell else {
                fatalError("Collection Search Cell not dequeued")
            }
            collectionCell.configure(photoUrl: viewModel.getSearchCollectionPhotoUrl(at: indexPath.row), collectionName: viewModel.getSearchCollectionName(at: indexPath.row))
            cell = collectionCell
            
        case 2:
            guard let userCell = tableView.dequeueReusableCell(withIdentifier: "SearchUserTableViewCell", for: indexPath) as? SearchUserTableViewCell else {
                fatalError("User Search Cell not dequeued")
            }
            userCell.configure(
                photoUrl: viewModel.getSearchUserProfilePhotoUrl(at: indexPath.row),
                userName: viewModel.getSearchUserUserName(at: indexPath.row),
                name: viewModel.getSearchUserUserName(at: indexPath.row)
            )
            cell = userCell
            
        default:
            fatalError("segment control error")
        }
        return cell
    }
}
extension SearchScreenViewController: SearchDelegate {
    func reload() {
        self.tableView.reloadData()
    }
}
    // MARK: - Search Bar Delegate
extension SearchScreenViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchQuery = searchBar.text ?? ""
        viewModel.searchPhotos(searchQuery: searchQuery)
        viewModel.searchCollections(searchQuery: searchQuery)
        viewModel.searchUsers(searchQuery: searchQuery)
    }
}
