//
//  SearchUserTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/28/21.
//

import UIKit

class SearchUserTableViewCell: UITableViewCell {
    @IBOutlet private weak var profileImage: UIImageView! {
        didSet {
            self.profileImage.layer.masksToBounds = true
            self.profileImage.layer.cornerRadius = self.frame.height / 4.0
        }
    }
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(photoUrl: String, userName: String, name: String) {
        self.profileImage.downloadImage(with: photoUrl)
        self.userNameLabel.text = userName
        self.nameLabel.text = name
    }
}
