//
//  SearchPhotoTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/28/21.
//

import UIKit

class SearchPhotoTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(photoUrl: String, name: String) {
        self.photoImageView.downloadImage(with: photoUrl)
        self.authorName.text = name
    }
}
