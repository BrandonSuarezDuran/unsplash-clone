//
//  SearchTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/18/21.
//

import UIKit

class DefaultSearchTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var categoryLabel: UILabel!

    func configure(category: String) {
        self.categoryLabel.text = category
    }
}
