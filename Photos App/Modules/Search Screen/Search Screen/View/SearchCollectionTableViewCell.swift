//
//  SearchCollectionTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 5/28/21.
//

import UIKit

class SearchCollectionTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView! {
        didSet {
            self.photoImageView.layer.masksToBounds = true
            self.photoImageView.layer.cornerRadius = self.frame.height / 10.0
        }
    }
    @IBOutlet private weak var collectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(photoUrl: String, collectionName: String) {
        self.photoImageView.downloadImage(with: photoUrl)
        self.collectionLabel.text = collectionName
    }
}
