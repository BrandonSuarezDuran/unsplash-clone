//
//  FirstTableViewCell.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/4/21.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImage: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(photoUrl: String, name: String) {
        self.photoImage.downloadImage(with: photoUrl)
        self.nameLabel.text = name
    }
}
