//
//  CollectionScreenViewModel.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/4/21.
//

import Foundation
import UIKit
protocol CollectionPhotosDelegate: AnyObject {
    func reload()
}

class CollectionPhotosScreenViewModel {
    let routerInstance: Router<EndPoint> = Router<EndPoint>()
    // MARK: - Data Source
    private var dataSource: [GetACollectionsPhotos]
    weak var delegate: CollectionPhotosDelegate?
    
    init(delegate: CollectionPhotosDelegate) {
        self.dataSource = []
        self.delegate = delegate
    }
    // MARK: - Functions
    func fetchCollectionPhotos(with id: String) {
        routerInstance.request(.getACollectionsPhotos(id: id)) { [weak self] (result: Result<[GetACollectionsPhotos], AppError>) in
            switch result {
            case .success(let data):
                self?.dataSource = data
                self?.delegate?.reload()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getCollectionPhotoUrl(at index: Int) -> String {
        guard let photoUrl = dataSource[index].urls?.small else {
            fatalError("Collection Screen Photo Url Not Found")
        }
        return photoUrl
    }
    
    func getCollectionPhotoName(at index: Int) -> String {
        guard let name = dataSource[index].user?.name else {
            fatalError("Collection Screen Photo Name Not Found")
        }
        return name
    }
    
    func getNumberOfRows() -> Int {
        dataSource.count
    }
}
