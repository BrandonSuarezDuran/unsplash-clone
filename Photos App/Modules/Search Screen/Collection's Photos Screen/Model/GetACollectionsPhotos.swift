//
//  GetACollectionsPhotos.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/3/21.
//

import Foundation

struct GetACollectionsPhotos: Decodable {
    var id: String?
    var createdAt: String?
    var updatedAt: String?
    var width: Int?
    var height: Int?
    var color: String?
    var blurHash: String?
    var likes: Int?
    var likedByUser: Bool
    var description: String?
    var user: GetACollectionUser?
    var currentUserCollections: [CurrentUserCollection]
    var urls: Urls?
    var links: GetACollectionsPhotosLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, width, height, color, likes, description, user, urls, links
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case blurHash = "blur_hash"
        case likedByUser = "liked_by_user"
        case currentUserCollections = "current_user_collections"
    }
}

struct GetACollectionUser: Decodable {
    var id: String?
    var username: String?
    var name: String?
    var portfolioUrl: String?
    var bio: String?
    var location: String?
    var totalLikes: Int?
    var totalCollections: Int?
    var instagramUsername: String?
    var twitterUsername: String?
    var profileImage: ProfileImage?
    var links: GetACollectionLinks?
    
    enum CodingKeys: String, CodingKey {
        case id, username, name, bio, location, links
        case portfolioUrl = "portfolio_url"
        case totalLikes = "total_likes"
        case totalCollections = "total_collections"
        case instagramUsername = "instagram_username"
        case twitterUsername = "twitter_username"
        case profileImage = "profile_image"
    }
}

struct GetACollectionLinks: Decodable {
    var itSelf: String?
    var html: String?
    var photos: String?
    var likes: String?
    var portfolio: String?
    
    enum CodingKeys: String, CodingKey {
        case html, photos, likes, portfolio
        case itSelf = "self"
    }
}

struct GetACollectionsPhotosLinks: Decodable {
    var itSelf: String?
    var html: String?
    var download: String?
    var downloadLocation: String?
    
    enum CodingKeys: String, CodingKey {
        case html, download
        case itSelf = "self"
        case downloadLocation = "download_location"
    }
}
