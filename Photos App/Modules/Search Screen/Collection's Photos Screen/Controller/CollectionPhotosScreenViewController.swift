//
//  CollectionScreenViewController.swift
//  unSplash Clone
//
//  Created by Brandon Suarez on 6/3/21.
//

import UIKit

class CollectionPhotosScreenViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var backGroundView: UIView!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    @IBOutlet private weak var curatedByLabel: UILabel!
    @IBOutlet private weak var nameButton: UIButton!
    // MARK: - Variables
    var collectionName: String = ""
    var curatedBy: String = ""
    var collectionID: String = ""
    // MARK: - View Model
    lazy var viewModel = CollectionPhotosScreenViewModel(delegate: self)
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameButton.setTitle(self.collectionName, for: .normal)
        self.curatedByLabel.text = self.curatedBy
        viewModel.fetchCollectionPhotos(with: collectionID)
    }
    
    @IBAction private func goBackButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func sharePhoto(_ sender: Any) {
        let shareApp = "https://unsplash.com"
        let activityActionBar = UIActivityViewController(activityItems: [shareApp], applicationActivities: [])
        activityActionBar.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
            present(activityActionBar, animated: true)
    }
}

extension CollectionPhotosScreenViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        guard let viewController = storyboard?.instantiateViewController(identifier: "ImageScreenViewController") as? PhotoScreenViewController else {
            fatalError("Image Screen ViewController Not Loaded")
        }
        viewController.modalPresentationStyle = .fullScreen
        viewController.photoUrl = viewModel.getCollectionPhotoUrl(at: index)
        viewController.name = viewModel.getCollectionPhotoName(at: index)
        self.present(viewController, animated: true, completion: nil)
    }
}

extension CollectionPhotosScreenViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let name = viewModel.getCollectionPhotoName(at: index)
        let photoUrl = viewModel.getCollectionPhotoUrl(at: index)
        var cell = UITableViewCell()
        
        if index == 0 {
            guard let firstCell = tableView.dequeueReusableCell(withIdentifier: "FirstTableViewCell", for: indexPath) as? ImageTableViewCell else {
                fatalError("Collection Screen First Cell not dequeued")
            }
            firstCell.configure(photoUrl: photoUrl, name: name)
            cell = firstCell
        } else {
            guard let secondCell = tableView.dequeueReusableCell(withIdentifier: "SecondTableViewCell", for: indexPath) as? ImageTableViewCell else {
                fatalError("Collection Screen Second Cell Not Dequeued")
            }
            secondCell.configure(photoUrl: photoUrl, name: name)
            cell = secondCell
        }
        return cell
    }
}

extension CollectionPhotosScreenViewController: CollectionPhotosDelegate {
    func reload() {
        self.tableView.reloadData()
    }
}
